<?php
use   yii\widgets\ActiveForm;
echo 'HOLA';
use yii\helpers\Html;

echo Url::to(['post/form', 'slug' => $post->slug]);
$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'],
]) 
?>
   <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'body')->textArea(['rows' => '6']) ?>
    <?= $form->field($model, 'sections_id')->dropdownList($sections,
                                ['prompt'=>'Selecciona una categoría']); ?>
    <?= $form->field($model,'image')->fileInput() ?>
<div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Agrega', ['class' => 'btn btn-primary']) ?>
        </div>
</div>
<?php ActiveForm::end() ?>
