<?php

use yii\db\Migration;

class m161226_183253_categories extends Migration
{
    public function up()
    {
        $this->createTable('categories',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->boolean(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],'engine innodb');
    }

    public function down()
    {
        echo "m161226_183253_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
