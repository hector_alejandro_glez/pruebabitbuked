<?php

use yii\db\Migration;

class m161228_183209_secciones_table_create extends Migration
{
    public function up()
    {

        $this->createTable('sections',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'status' => $this->boolean(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],'engine innodb');
        
    }

    public function down()
    {
        echo "m161228_183209_secciones_table_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
