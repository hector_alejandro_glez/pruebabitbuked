<?php

use yii\db\Migration;

class m161226_183303_products extends Migration
{
    private $tableName = "products";
    public function up()
    {

        $this->createTable($this->tableName,[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'price' => $this->money(10,2)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],'engine innodb'
                );

        $this->createIndex(
            'idx-products-category_id',
            'products',
            'category_id'
        );

        $this->addForeignKey(
            'fk-products-category_id',
            'products',
            'category_id',
            'categories',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        echo "m161226_183303_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
